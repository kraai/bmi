# Prerequisites

* [Git](https://git-scm.com/)
* [Rust](https://rustup.rs/)

# Instructions

1. Run `git clone https://gitlab.com/kraai/bmi.git`.
2. Run `cd bmi`.
3. Run `cargo install --path .`.
4. Create a `bmi.conf` file in your configuration directory containing
   `birthday = BIRTHDAY` and `height = HEIGHT`, where `BIRTHDAY` is
   your birthday in ISO-8601 format and `HEIGHT` is your height in
   inches.
5. Run `bmi WEIGHT`, where `WEIGHT` is your weight in pounds.
