// Copyright 2018 Matt Kraai

// This file is part of bmi.

// bmi is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
// General Public License as published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// bmi is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero
// General Public License for more details.

// You should have received a copy of the GNU Affero General Public License along with bmi.  If not,
// see <https://www.gnu.org/licenses/>.

extern crate bmi;
extern crate chrono;
#[macro_use]
extern crate clap;
extern crate dirs;
#[macro_use]
extern crate serde_derive;
extern crate toml;

use chrono::{Datelike, Local, NaiveDate};
use clap::{App, Arg};
use std::fs;
use toml::value::Datetime;

#[derive(Deserialize)]
struct Config {
    height: u8,
    birthday: Datetime,
}

fn is_f64(v: String) -> Result<(), String> {
    v.parse::<f64>().map(|_| ()).map_err(|e| e.to_string())
}

fn main() {
    let matches = App::new(crate_name!())
        .version(crate_version!())
        .author(crate_authors!())
        .about("Calculate body-mass index (BMI).")
        .arg(
            Arg::with_name("WEIGHT")
                .help("Weight in pounds")
                .required(true)
                .validator(is_f64),
        )
        .get_matches();
    let mut path = dirs::config_dir().unwrap();
    path.push("bmi.toml");
    let config: Config = toml::from_str(&fs::read_to_string(path).unwrap()).unwrap();
    let weight = matches.value_of("WEIGHT").unwrap().parse::<f64>().unwrap();
    let result = bmi::calculate(config.height as f64, weight);
    let birthday = config.birthday.to_string().parse::<NaiveDate>().unwrap();
    let today = Local::today();
    let age = if today.month() > birthday.month()
        || (today.month() == birthday.month() && today.day() >= birthday.day())
    {
        today.year() - birthday.year()
    } else {
        today.year() - birthday.year() - 1
    } as u8;
    println!("{} ({:?})", result, bmi::status(result, age));
}
