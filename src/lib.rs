// Copyright 2018 Matt Kraai

// This file is part of bmi.

// bmi is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
// General Public License as published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// bmi is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero
// General Public License for more details.

// You should have received a copy of the GNU Affero General Public License along with bmi.  If not,
// see <https://www.gnu.org/licenses/>.

pub fn calculate(height: f64, weight: f64) -> f64 {
    weight / (height * height) * 703.
}

#[derive(Debug, Eq, PartialEq)]
pub enum Status {
    Underweight,
    Healthy,
    Overweight,
    Obese,
}

pub fn status(bmi: f64, age: u8) -> Status {
    if age >= 20 {
        if bmi < 18.5 {
            Status::Underweight
        } else if bmi < 25. {
            Status::Healthy
        } else if bmi < 30. {
            Status::Overweight
        } else {
            Status::Obese
        }
    } else {
        unimplemented!()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_calculate() {
        // https://www.nhlbi.nih.gov/health/educational/lose_wt/BMI/bmicalc.htm
        let expected = 25.9;
        let actual = calculate(67., 165.4);
        assert!(expected - 0.05 <= actual && actual < expected + 0.05);
    }

    #[test]
    fn test_underweight() {
        assert_eq!(status(18.4, 20), Status::Underweight);
    }

    #[test]
    fn test_healthy() {
        assert_eq!(status(18.5, 20), Status::Healthy);
        assert_eq!(status(24.9, 20), Status::Healthy);
    }

    #[test]
    fn test_overweight() {
        assert_eq!(status(25., 20), Status::Overweight);
        assert_eq!(status(29.9, 20), Status::Overweight);
    }

    #[test]
    fn test_obese() {
        assert_eq!(status(30., 20), Status::Obese);
    }
}
